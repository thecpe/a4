# LIS4368 - Advancced Web Applications Development

## Christian Pelaez-Espinosa

### Assignment 4#

> This assignment was focused on **Model View Controller (MVC)** which is a framework that divides an application's implementation into three
discrete component roles: models, views, and controllers.

* Model: - The “business” layer. how do we represent the data? Defines business objects, and provides
methods for business processing (e.g., Customer and CustomerDB classes).

* Controller: Directs the application “flow”—using servlets. Generally, reads parameters sent from
requests, updates the model, saves data to the data store, and forwards updated model to JSPs for
presentation (e.g., CustomerListServlet).

* View: How do I render the result? Defines the presentation—using .html or .jsp files (e.g., index.html,
index.jsp, thanks.jsp).

#### Assignment #4 ScreenShots:

ScreenShot 1: *Failed Validation* ![Screen Shot 2016-03-23 at 2.04.04 PM.png](https://bitbucket.org/repo/xp6koy/images/2317424985-Screen%20Shot%202016-03-23%20at%202.04.04%20PM.png)


ScreenShot 2: *Passed Validation* ![Screen Shot 2016-03-24 at 2.10.46 AM.png](https://bitbucket.org/repo/xp6koy/images/1101851143-Screen%20Shot%202016-03-24%20at%202.10.46%20AM.png)